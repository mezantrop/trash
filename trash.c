/*
 * Copyright (c) 2020 Mikhail E. Zakharov <zmey20000@yahoo.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *    in this position and unchanged.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* -----------------------------------------------------------------------------
TraSh - a simple IP Traffic Show program that saves data into SQLite 3 database
----------------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pcap/pcap.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <time.h>
#include <sqlite3.h>


struct tp_ports {
        u_short	sport;		/* source port */
        u_short dport;     	/* destination port */
};

sqlite3 *db;			/* SQlite3 database handle */

/* -------------------------------------------------------------------------- */
void usage(void);
void got_packet(u_char *args, const struct pcap_pkthdr *header,
	const u_char *packet);
int db_save_vector(sqlite3 *db, char *af, char *ip_proto,
	char *ip1, u_short ip1_port, u_short ip1_datalen,
	char *ip2, u_short ip2_port, u_short ip2_datalen);

/* -------------------------------------------------------------------------- */
int main(int argc, char *argv[]) {
	char *dev = NULL, errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *allifs;
	pcap_t *handle;

	struct bpf_program fp;		/* The compiled filter expression */
       	char filter_exp[] = "ip || ip6"; /* The filter expression */
	bpf_u_int32 mask;		/* The netmask of the sniffing device */
	bpf_u_int32 net;		/* The IP of the sniffing interface */


	if (argv[1] && strncmp(argv[1], "-h", 2) == 0) usage();

	if (argv[1]) {
		printf("Using specified interface: ");
		dev = argv[1];
	} else {
		printf("Auto-detecting interface: ");
		if (pcap_findalldevs(&allifs, errbuf) == -1) {
			fprintf(stderr, "Couldn't find interfaces: %s\n",
				errbuf);
			exit(1);
		}
		if (allifs)
			dev = allifs->name;

	}

	printf("%s\n", dev);

	handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
       	if (handle == NULL) {
       		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
       		exit(1);
       	}

	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
	 	fprintf(stderr, "Can't get netmask for device %s\n", dev);
	 	net = 0;
	 	mask = 0;
	}

 	if (pcap_compile(handle, &fp, filter_exp, 0, mask) == -1) {
 		fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp,
			pcap_geterr(handle));
 		exit(2);
 	}
 	if (pcap_setfilter(handle, &fp) == -1) {
 		fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp,
			pcap_geterr(handle));
 		exit(3);
 	}

	if (sqlite3_open("./trash.sqlite3", &db) != SQLITE_OK) {
		fprintf(stderr, "Can't open SQLite3 database: %s\n",
			sqlite3_errmsg(db));
		sqlite3_close(db);
		exit(4);
	}

	pcap_loop(handle, -1, got_packet, NULL);

	pcap_freealldevs(allifs);
	pcap_freecode(&fp);
	pcap_close(handle);
	sqlite3_close(db);

	exit(0);
}

/* -------------------------------------------------------------------------- */
void usage(void) {
	printf("TraSh - a simple Trafic Show program that saves data into SQLite 3 database\n\n");
	printf("Usage:\n\ttrash [interface] [/path/to/trash.sqlite3]\n");
	printf("\ttrash -h\n\n");
	printf("The default database file is ./trash.sqlite3.\n");
	printf("If interface is omitted, trash tries to autodetect it.\n\n");
	exit(0);
}

/* -------------------------------------------------------------------------- */
void got_packet(u_char *args, const struct pcap_pkthdr *header,
	const u_char *packet) {

	const struct ether_header *ethernet;	/* The ethernet header */
	const struct ip *ipv4;			/* The IP header */
	const struct ip6_hdr *ipv6;		/* The IPv6 header */
	const struct tp_ports *tp = NULL;	/* The UDP/TCP ports */

	u_short ether_type;			/* Ethernet type: IP|IPv6|etc */
	u_int ip_hl;				/* IP header length */
	u_char nexthdr;				/* Protocol / Next Header */

	char ip1[INET6_ADDRSTRLEN], ip2[INET6_ADDRSTRLEN];
	u_short ip1_port = 0, ip2_port = 0;
	u_short ip1_datalen = 0, ip2_datalen = 0;
	struct protoent *ip_pent;


	ethernet = (struct ether_header *)(packet);
	ether_type = ntohs(ethernet->ether_type);
	switch (ether_type) {
		case ETHERTYPE_IP:
			ipv4 = (struct ip *)(packet + ETHER_HDR_LEN);
			ip_hl = ipv4->ip_hl * 4;
			if (ip_hl < 20)	return;	/* Skip corrupted packet */

			nexthdr = ipv4->ip_p;
			if (nexthdr == IPPROTO_TCP || nexthdr == IPPROTO_UDP)
				tp = (struct tp_ports *)(packet +
					ETHER_HDR_LEN + ip_hl);

			if (ipv4->ip_src.s_addr <= ipv4->ip_dst.s_addr) {
				inet_ntop(AF_INET, &ipv4->ip_src, ip1, sizeof(ip1));
				inet_ntop(AF_INET, &ipv4->ip_dst, ip2, sizeof(ip2));
				if (tp) {
					ip1_port = ntohs(tp->sport);
					ip2_port = ntohs(tp->dport);
				}
				ip1_datalen = ntohs(ipv4->ip_len);
			} else {
				inet_ntop(AF_INET, &ipv4->ip_src, ip2, sizeof(ip2));
				inet_ntop(AF_INET, &ipv4->ip_dst, ip1, sizeof(ip1));
				if (tp) {
					ip2_port = ntohs(tp->sport);
					ip1_port = ntohs(tp->dport);
				}
				ip2_datalen = ntohs(ipv4->ip_len);
			}
			break;

		case ETHERTYPE_IPV6:
			ipv6 = (struct ip6_hdr *)(packet + ETHER_HDR_LEN);
			nexthdr = ipv6->ip6_ctlun.ip6_un1.ip6_un1_nxt;
			if (nexthdr == IPPROTO_TCP || nexthdr == IPPROTO_UDP)
				tp = (struct tp_ports *)(packet +
					ETHER_HDR_LEN + sizeof(struct ip6_hdr));

			if (memcmp(&ipv6->ip6_src, &ipv6->ip6_dst, sizeof(struct in6_addr)) <= 0) {
				inet_ntop(AF_INET6, &ipv6->ip6_src, ip1, sizeof(ip1));
				inet_ntop(AF_INET6, &ipv6->ip6_dst, ip2, sizeof(ip2));
				if (tp) {
					ip1_port = ntohs(tp->sport);
					ip2_port = ntohs(tp->dport);
				}
				ip1_datalen = ntohs(ipv6->ip6_ctlun.ip6_un1.ip6_un1_plen);
			} else {
				inet_ntop(AF_INET6, &ipv6->ip6_src, ip2, sizeof(ip2));
				inet_ntop(AF_INET6, &ipv6->ip6_dst, ip1, sizeof(ip1));
				if (tp) {
					ip2_port = ntohs(tp->sport);
					ip1_port = ntohs(tp->dport);
				}
				ip2_datalen = ntohs(ipv6->ip6_ctlun.ip6_un1.ip6_un1_plen);
			}
			break;

		default:			/* Skip non-IP packets */
			return;
	}

	/* determine protocol: TCP|UDP|ICMP|etc */
	ip_pent = getprotobynumber(nexthdr);
	db_save_vector(db, ether_type == ETHERTYPE_IP ? "IPv4" : "IPv6",
		ip_pent->p_aliases[0],
		ip1, ip1_port, ip1_datalen,
		ip2, ip2_port, ip2_datalen);

	return;
}

/* -------------------------------------------------------------------------- */
int db_save_vector(sqlite3 *db, char *af, char *ip_proto,
	char *ip1, u_short ip1_port, u_short ip1_datalen,
	char *ip2, u_short ip2_port, u_short ip2_datalen) {

	time_t c_time;
	char *db_errmsg = 0;
	char sql_statement[512];
	char *sql_insert = "INSERT INTO Vector(AF, Protocol, "
			"IP1, IP1_port, IP1_count, IP1_datalen, "
			"IP2, IP2_port, IP2_count, IP2_datalen, "
			"C_time, A_time) "
		"VALUES('%s', '%s', '%s', %d, %d, %d, '%s', %d, %d, %d, %d, %d);";
	char *sql_select = "SELECT id FROM Vector WHERE "
		"IP1 = '%s' and IP2='%s' and IP1_port = %d and IP2_port = %d LIMIT 1;";
	char *sql_update = "UPDATE Vector SET A_time = %d, "
		"IP1_count = IP1_count + %d, IP1_datalen = IP1_datalen + %d, "
		"IP2_count = IP2_count + %d, IP2_datalen = IP2_datalen + %d "
		"WHERE id = %d;";
	sqlite3_stmt *res;
	int db_rc;

	snprintf(sql_statement, sizeof(sql_statement), sql_select,
		ip1, ip2, ip1_port, ip2_port);
	if (sqlite3_prepare_v2(db, sql_statement, -1, &res, 0) != SQLITE_OK) {
		fprintf(stderr, "Failed to prepare SQL statement: %s\n",
			sqlite3_errmsg(db));
		sqlite3_close(db);
		exit(5);
	}

	c_time = time(NULL);
	switch ((db_rc = sqlite3_step(res))) {
		case SQLITE_DONE:		/* Must INSERT */
			snprintf(sql_statement, sizeof(sql_statement),
				sql_insert, af, ip_proto,
				ip1, ip1_port, ip1_datalen ? 1 : 0, ip1_datalen,
				ip2, ip2_port, ip2_datalen ? 1 : 0, ip2_datalen,
				c_time, c_time);
			break;
		case SQLITE_ROW:		/* Must UPDATE */
			snprintf(sql_statement, sizeof(sql_statement),
				sql_update, c_time,
				ip1_datalen ? 1 : 0, ip1_datalen,
				ip2_datalen ? 1 : 0, ip2_datalen,
				sqlite3_column_int64(res, 0));
			break;
		default:
			fprintf(stderr, "Failed to fetch data: %d\n", db_rc);
			exit(6);
	}

	while ((db_rc = sqlite3_exec(db, sql_statement, 0, 0, &db_errmsg)) == SQLITE_BUSY)
		usleep(3000);

	sqlite3_finalize(res);

	if (db_rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", db_errmsg);
		fprintf(stderr, "SQL statement: %s\n", sql_statement);
		sqlite3_free(db_errmsg);
		sqlite3_close(db);
		exit(6);
	}

	return 0;
}
