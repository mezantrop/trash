TraSh - a simple IP Traffic Show program that saves data into SQLite 3 database

Usage:
	trash [interface] [/path/to/trash.sqlite3]

The default database file is ./trash.sqlite3.
If interface is omitted, trash tries to autodetect it.
