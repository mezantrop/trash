CC = cc
CFLAGS = -O2 -pipe -fstack-protector -fno-strict-aliasing -I/usr/local/include
LDFLAGS = -lpcap -lsqlite3 -L/usr/local/lib

PREFIX = /usr/local
BINDIR = /bin
SHAREDIR = /share/trash

all:
	${CC} ${CFLAGS} -Wall ${LDFLAGS} -o trash trash.c

install: all
	install -d -m 755 ${PREFIX}${BINDIR}
	install -d -m 755 ${PREFIX}${SHAREDIR}
	install -m 755 trash ${PREFIX}${BINDIR}
	install -m 755 README.txt ${PREFIX}${SHAREDIR}

deinstall:
	rm ${PREFIX}${BINDIR}/trash
	rm ${PREFIX}${SHAREDIR}/README.txt

uninstall:	deinstall

clean:
	rm -f trb_capd
	rm -f *.o core *~ a.out *.core
